﻿using EmployeeMaintenance.API.Interfaces;
using EmployeeMaintenance.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeMaintenance.API
{
    public class PersonService : IPersonService
    {
        private HttpClient _client;
        public IEnumerable<PersonModel> People { get; set; }

        public PersonService()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri("https://techtestapi.azurewebsites.net/")
            };
        }

        public async Task CreatePerson(PersonModel person)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(person));
                await _client.PostAsync("Api/People/", content);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task DeletePerson(int personId)
        {
            try
            {
                var result = await _client.DeleteAsync("Api/People/"+ personId.ToString());
                if(result.IsSuccessStatusCode)
                {
                    return;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<IEnumerable<PersonModel>> GetPeople()
        {
            try
            {
                var result = await _client.GetStringAsync("Api/People");
                if (result != null)
                {
                    People = JsonConvert.DeserializeObject<IEnumerable<PersonModel>>(result);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return People;
        }

        public void UpdatePerson(PersonModel person)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(person));
                _client.PostAsync("Api/People/", content);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
