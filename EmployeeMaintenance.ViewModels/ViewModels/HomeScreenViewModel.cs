﻿using EmployeeMaintenance.API.Interfaces;
using EmployeeMaintenance.Models;
using MvvmCross.ViewModels;
using System.Collections.Generic;

namespace EmployeeMaintenance.ViewModels.ViewModels
{
    public class HomeScreenViewModel : MvxViewModel
    {
        public string Title => "Employee Manager";

        private IEmployeeService _employeeService;
        private IPersonService _personService;

        public IEnumerable<PersonModel> People
        {
            get => _people;
            set => SetProperty(ref _people, value);
        }
        private IEnumerable<PersonModel> _people;

        public IEnumerable<EmployeeModel> Employees
        {
            get => _employees;
            set => SetProperty(ref _employees, value);
        }
        private IEnumerable<EmployeeModel> _employees;

        public HomeScreenViewModel(IEmployeeService employeeService, IPersonService personService)
        {
            _employeeService = employeeService;
            _personService = personService;

            InitializeServices();
        }

        private async void InitializeServices()
        {
            People = await _personService.GetPeople();
            Employees = await _employeeService.GetEmployees();
        }
    }
}
