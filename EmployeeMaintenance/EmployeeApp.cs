﻿using System;
using Android.App;
using Android.Runtime;
using EmployeeMaintenance.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace EmployeeMaintenance
{
    [Application]
    public class EmployeeApp : MvxAppCompatApplication<MvxAppCompatSetup<App>, App>
    {
        public EmployeeApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
    }
}