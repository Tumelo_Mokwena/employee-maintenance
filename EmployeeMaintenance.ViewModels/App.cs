﻿using EmployeeMaintenance.API;
using EmployeeMaintenance.API.Interfaces;
using EmployeeMaintenance.ViewModels.ViewModels;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace EmployeeMaintenance.ViewModels
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            RegisterCustomAppStart<CustomAppStart<ExtendedSplashScreenViewModel>>();
            Mvx.RegisterSingleton<IEmployeeService>(new EmployeeService());
            Mvx.RegisterSingleton<IPersonService>(new PersonService());
        }
    }
    public class CustomAppStart<TViewModel> : MvxAppStart<TViewModel>
        where TViewModel : IMvxViewModel
    {
        public CustomAppStart(IMvxApplication application, IMvxNavigationService navigationService) : base(application, navigationService)
        {
        }

        protected override void NavigateToFirstViewModel(object hint)
        {
            NavigationService.Navigate<TViewModel>();
        }
    }
}
