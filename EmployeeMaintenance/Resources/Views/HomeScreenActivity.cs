﻿using Android.App;
using Android.OS;
using Android.Widget;
using EmployeeMaintenance.ViewModels.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeMaintenance.Resources.Views
{
    [Activity(Label = "Home Screen",
              Theme = "@style/AppTheme.NoActionBar")]
    public class HomeScreenActivity : MvxAppCompatActivity<HomeScreenViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.home_screen);

            List<string> people = ViewModel.People.Select(x => x.FirstName).ToList();

            ArrayAdapter adapter = new ArrayAdapter<string>(this, Resource.Layout.persons_list, people);

            ListView listView = (ListView)FindViewById(Resource.Id.personsList);
            listView.SetAdapter(adapter);
        }
    }
}