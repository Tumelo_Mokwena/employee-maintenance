﻿using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace EmployeeMaintenance.ViewModels.ViewModels
{
    public class ExtendedSplashScreenViewModel : MvxViewModel
    {
        private IMvxNavigationService _navigationService;

        public ExtendedSplashScreenViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;

            _navigationService.Navigate<HomeScreenViewModel>();
        }
    }
}
