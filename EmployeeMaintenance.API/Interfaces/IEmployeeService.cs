﻿using EmployeeMaintenance.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeMaintenance.API.Interfaces
{
    public interface IEmployeeService
    {
        Task CreateEmployee(EmployeeModel employee);
        Task<IEnumerable<EmployeeModel>> GetEmployees();
        void UpdateEmployee(EmployeeModel employee);
        Task DeleteEmployee(int employeeId);
    }
}
