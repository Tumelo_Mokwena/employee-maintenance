﻿using EmployeeMaintenance.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeMaintenance.API.Interfaces
{
    public interface IPersonService
    {
        Task CreatePerson(PersonModel person);
        Task<IEnumerable<PersonModel>> GetPeople();
        void UpdatePerson(PersonModel person);
        Task DeletePerson(int personId);
    }
}
