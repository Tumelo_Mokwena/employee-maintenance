﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeMaintenance.Models
{
    public class EmployeeModel
    {
        public int EmployeeId { get; set; }
        public int PersonId { get; set; }
        public string EmployeeNum { get; set; }
        public DateTime EmployeeDate { get; set; }
        public DateTime TerminatedDate { get; set; }
    }
}
