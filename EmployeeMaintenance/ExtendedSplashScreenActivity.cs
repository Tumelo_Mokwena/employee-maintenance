﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace EmployeeMaintenance
{
    [Activity(Label = "Employee Maintenance", 
              Theme = "@style/AppTheme.NoActionBar", 
              MainLauncher = true,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ExtendedSplashScreenActivity : MvxSplashScreenAppCompatActivity
    {
        public ExtendedSplashScreenActivity() : base(Resource.Layout.splash_screen)
        {
        }
    }
}