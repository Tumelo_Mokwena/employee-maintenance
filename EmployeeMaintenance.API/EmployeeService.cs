﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using EmployeeMaintenance.API.Interfaces;
using EmployeeMaintenance.Models;
using Newtonsoft.Json;

namespace EmployeeMaintenance.API
{
    public class EmployeeService : IEmployeeService
    {
        private HttpClient _client;
        public IEnumerable<EmployeeModel> Employees { get; set; }

        public EmployeeService()
        {
            _client = new HttpClient
            {
                BaseAddress = new System.Uri("https://techtestapi.azurewebsites.net/")
            };
        }

        public async Task CreateEmployee(EmployeeModel employee)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(employee));
                await _client.PostAsync("Api/Employees/", content);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task DeleteEmployee(int employeeId)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(employeeId.ToString()));
                await _client.DeleteAsync("Api/Employees/");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<IEnumerable<EmployeeModel>> GetEmployees()
        {
            try
            {
                var result = await _client.GetStringAsync("Api/Employees");
                if(result != null)
                {
                    Employees = JsonConvert.DeserializeObject<IEnumerable<EmployeeModel>>(result);
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            return Employees;
        }

        public void UpdateEmployee(EmployeeModel employee)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(employee));
                _client.PostAsync("Api/Employees/", content);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
